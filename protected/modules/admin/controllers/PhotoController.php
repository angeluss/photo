<?php

class PhotoController extends Controller
{
	public $layout = '/layouts/column2';

	public function filters()
	{
		return array('accessControl',);
	}

	public function accessRules()
	{
		return array(
			array(
                'allow',
                'actions' => array('index','view','create','update', 'deletefile', 'delete', 'up', 'down'),
                'users' => array('@'),),
			array('deny', 'users' => array('*'),),
		);
	}

	public function actionView($id)
	{
		$this->render('view',array('model' => $this->loadModel($id),));
	}

    public function initSave (Photo $model)
    {
        if (isset($_POST['Photo'])) {
            $oldImg = $model->url;
            $model->attributes = $_POST['Photo'];
            $uploadFile = CUploadedFile::getInstance($model, 'url');

            if ($uploadFile !== null) {
                $model->url = $uploadFile;
            }

            if ($model->save()) {

                if (!empty($model->url)) {

                    if (is_file('images/' . $oldImg)) {
                        unlink('images/' . $oldImg);
                    }

                    $model->url->saveAs('images/' . $model->url->getName());
                }
                $this->redirect(array('index'));
            }
        }
    }

	public function actionCreate()
	{
		$model = new Photo;
		$this->initSave($model);
		$this->render('create',array('model' => $model,));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
        $this->initSave($model);
		$this->render('update',array('model' => $model,));
	}

	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
            $model = $this->loadModel($id);

            if ($model->delete()) {

                if (is_file('images/'.$model->url)) {
                    unlink('images/'.$model->url);
                }

                if (!isset($_GET['ajax'])) {
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                }
            }
		}
		else {
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }
	}

    public function actionDeletefile($id)
    {
        $model = $this->loadModel($id);

        if (is_file('images/'.$model->url)) {
            unlink('images/'.$model->url);

        }

        $model->url = '';
        $model->save();
        $this->redirect('/admin/photo/update/id/'.$model->ID);
    }

	public function actionIndex()
	{
        $model = new Photo;
		$this->render('index',array('model' => $model,));
	}

	public function loadModel($id)
	{
		$model = Photo::model()->findByPk($id);
		if($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
        }
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax'] === 'photo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

    function removeDirectory($dir)
    {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? removeDirectory($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }

    public function actionUp ($curPos, $curId)
    {
        $change = Yii::app()->db->createCommand()
            ->select('ID, position')
            ->from('photo')
            ->where('position < :pos', array(':pos' => $curPos))
            ->order('position desc')
            ->queryRow();

        if ($change) {
            $query = Yii::app()->db->createCommand(
                "UPDATE `photo` SET position='" . $change['position'] . "' WHERE `ID`=" . $curId);
            $query->execute();

            $query1 = Yii::app()->db->createCommand(
                "UPDATE `photo` SET position='" . $curPos . "' WHERE `ID`=" . $change['ID']);
            $query1->execute();
        }
        $this->redirect(array('index'));
    }

    public function actionDown ($curPos, $curId)
    {
        $change = Yii::app()->db->createCommand()
            ->select('ID, position')
            ->from('photo')
            ->where('position > :pos', array(':pos' => $curPos))
            ->order('position')
            ->queryRow();

        if ($change) {
            $query = Yii::app()->db->createCommand(
                "UPDATE `photo` SET position='" . $change['position'] . "' WHERE `ID`=" . $curId);
            $query->execute();

            $query1 = Yii::app()->db->createCommand(
                "UPDATE `photo` SET position='" . $curPos . "' WHERE `ID`=" . $change['ID']);
            $query1->execute();
        }
        $this->redirect(array('index'));
    }

}
