<?php
$this->breadcrumbs = array(
	'Photos',
);
?>

<?php $this->widget(
    'booster.widgets.TbGridView',
    array(
	    'dataProvider' => $model->search(),
        'template' => "{items}\n{pager}",
        'columns' => array(
            'position',
            array(
                'name' => 'Image',
                'value' => 'CHtml::image(
                                    "/images/".$data->url,
                                    "this is alt tag of image",
                                    array("width" => "120px" ,"height" => "120px")
                                )',
                'type' => 'raw',
            ),
            'title',
            array(
                'class' => 'CButtonColumn',
                'htmlOptions' => array ('width' => '9%'),
                'buttons' => array(
                    'up' => array(
                        'label' => 'Move up',
                        'imageUrl' => '/icons/up.png',
                        'url' => 'Yii::app()->createUrl(
                            "admin/photo/up",
                            array(
                                "curId" => $data->ID,
                                "curPos" => $data->position
                            )
                        )',
                    ),
                    'down' => array(
                        'label' => 'Move down',
                        'imageUrl' => '/icons/down.png',
                        'url' => 'Yii::app()->createUrl(
                            "admin/photo/down",
                            array(
                                "curId" => $data->ID,
                                "curPos" => $data->position
                            )
                        )',
                    ),
                ),
                'header' => yii::t('core','Actions'),
//
                'template' => '{up}{down}{update}{delete}',
            ),
        )
));
echo CHtml::link('Add new banner', array('photo/create'), array('class' => 'btn'));
?>
