<div class="form">

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'photo-form',
	'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size' => 60,'maxlength' => 255)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>

    <div class="control-group">
        <?php if (!empty($model->url)): ?>
            Image:<br>
            <?php
            echo CHtml::image(
                "/images/".$model->url,
                "this is alt tag of image"
            );
            ?>

            <?php if (!$model->isNewRecord): ?>
                <?php if ($model->url != ''): ?>
                    <p>
                        <?php
                            echo CHtml::link(
                                'Delete image',
                                array("/admin/photo/deletefile/", 'id' => $model->ID),
                                array('confirm' => 'Are you sure?',)
                            );
                        ?>
                    </p>
                <?php endif; ?>
            <?php endif; ?>

        <?php else: ?>
            No Picture uploaded
        <?php endif; ?>

        <br>
        <p>Upload new image: Formats: JPG, JPEG, GIF, PNG.</p>
        <?php echo $form->fileField($model,'url'); ?>

        <?php echo $form->error($model,'url'); ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->