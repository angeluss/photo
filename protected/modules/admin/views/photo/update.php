<?php
$this->breadcrumbs = array(
	'Photos' => array('index'),
	$model->title => array('view','id' => $model->ID),
	'Update',
);

?>

<h1>Update photo <?php echo $model->ID; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>