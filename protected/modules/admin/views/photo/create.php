<?php
$this->breadcrumbs = array(
	'Photos' => array('index'),
	'Create',
);

?>

<h1>Add new photo</h1>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>