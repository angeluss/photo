<?php

class Photo extends CActiveRecord
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'photo';
	}

	public function rules()
	{
		return array(
			array('title', 'required'),
			array('title', 'length', 'max' => 255),
            array('url', 'file', 'allowEmpty' => true, 'types' => 'jpg, jpeg, gif, png'),
			array('ID, title, url, position', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array();
	}

	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'title' => 'Title',
			'url' => 'Url',
			'position' => 'Position',
		);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('position',$this->position);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'position',
            )
		));
	}

    public static function getImages()
    {
        $items = Yii::app()->db->createCommand()
            ->select('url, title')
            ->from('photo')
            ->order('position')
            ->queryAll();

        return $items;
    }

    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            $position = Yii::app()->db->createCommand()
                ->select('MAX(position)')
                ->from('photo')
                ->queryAll();

            $position = $position[0]['MAX(position)'];

            $this->position = $position+1;
        }
        return parent::beforeSave();
    }
}