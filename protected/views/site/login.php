<h1>Please login</h1>


<div class="form" >
<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'login-form',
    'type' => 'horizontal',
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
    'htmlOptions' => array('class' => 'well'),
)); ?>

	<div class="row">
<!--		--><?php //echo $form->labelEx($model,'username'); ?>
    Username:
		<?php echo $form->textField($model,'username'); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
<!--		--><?php //echo $form->labelEx($model,'password'); ?>
    Password:
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>


	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Login'); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
