<?php $this->pageTitle = Yii::app()->name; ?>

<div class="flexslider">
    <ul class="slides">
        <?php foreach ($model as $item): ?>
        <li>
            <img src="images/<?php echo $item['url']; ?>" />
        </li>
        <?php endforeach; ?>
    </ul>
</div>