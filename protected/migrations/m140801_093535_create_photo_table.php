<?php

class m140801_093535_create_photo_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('photo', array(
            'ID' => 'pk',
            'title' => 'string NOT NULL',
            'url' => 'string NULL',
            'position' => 'int NULL',
        ));
	}

	public function down()
	{
        $this->dropTable('photo');
	}

}