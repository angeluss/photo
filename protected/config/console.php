<?php
$db = include 'database.php';
return array(
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'My Console Application',

	'components' => array(
		'db' => $db,
	),
);