<?php

$db = include 'database.php';
return array(
	'basePath' => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name' => 'Photo Gallery',

	'preload' => array('log', 'booster'),

	'import' => array(
		'application.models.*',
		'application.components.*',
	),

	'modules' => array(
        'admin',
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => '123',
//			'ipFilters' => array('127.0.0.1','::1'),
		),
	),

	'components' => array(
		'user' => array(
			'allowAutoLogin' => true,
		),

		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false,
			'rules' => array(
                '/' => 'site/index',

				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',

                '<module:\w+>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/index',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
			),
		),

		'db' => $db,

		'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
				/*
				array(
					'class' => 'CWebLogRoute',
				),
				*/
			),
		),
        'booster' => array(
            'class' => 'ext.booster.components.Booster',
        ),
	),

	'params' => array(
		'adminEmail' => 'webmaster@example.com',
	),
);